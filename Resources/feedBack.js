var win = Ti.UI.currentWindow;

var b = Ti.UI.createLabel({
	text : 'Stäng',
	color : '#fff',
	top : '20dp',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function() {

	win.hide();
});

var imageHolder=Ti.UI.createView({
	
	backgroundColor:'transparent',
	width:'250dp',
	top:'50dp'
});

win.add(imageHolder);

var image = Ti.UI.createImageView({
	top : '50dp',
	width : '200dp',
	height : '200dp',
	image : 'images/fax.png',

});

imageHolder.add(image);

image.addEventListener('click', function() {
	
	var emailDialog = Ti.UI.createEmailDialog();
	emailDialog.subject = "Feedback från Klagshamsskolans App";
	emailDialog.toRecipients = ['oklintman@icloud.com'];

	emailDialog.open();
});



var feedbackText=  Ti.UI.createLabel({
	
	top:'280dp',
	text: 'Tryck på faxen för att skicka feedback om vad du tycker om appen eller om du saknar något... ',
	color:'#fff',
	textAlign:'center',
	font: {fontFamily: 'Snell Roundhand', fontSize:24, fontWeight: 'bold'}
	
});

imageHolder.add(feedbackText);






