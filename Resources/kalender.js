var win=Ti.UI.currentWindow;

var b = Ti.UI.createLabel({
	text : 'Stäng',
	color : '#fff',
	top : '20dp',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function() {

	win.hide();
});

var calenderTableView=Ti.UI.createTableView({
	
	backgroundColor : 'transparent',
	//separatorStyle : Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
	top : '50dp',

});

win.add(calenderTableView);


var dataArray=[];
var curr=0;

function addToDataArray (dateMonth, dateDay, titleOnEvent, text){
	
	var row= Ti.UI.createTableViewRow({
		height:'100dp',
		width:'320dp',
		selectedBackgroundColor:'transparent',
		
	});
	
	
	
	var calenderImage= Ti.UI.createImageView({
		image:'icons/kalenderIcon.png',
		left:'5dp',
		height:'80dp',
		width:'80dp',
		layout:'vertical',
		top: '5dp'
	});
	
	
	row.add(calenderImage);
	
	
	var dateMonth= Ti.UI.createLabel({
		text:dateMonth,
		font:{fontSize:22, fontWeight:'bold'},
		top:'8dp',
		//left:'0dp',
		color:'black'
	});
	
	calenderImage.add(dateMonth);
	
	var dateDay=Ti.UI.createLabel({
		text:dateDay,
		font:{fontSize:22, fontWeight: 'bold'},
		top:'5dp',
		//left:'0dp',
		color:'black'
			
	});
	
	calenderImage.add(dateDay);
	
	
	
	
	var title= Ti.UI.createLabel({
		text: titleOnEvent,
		font:{fontSize:18, fontWeight:'bold'},
		top:'5dp',
		left: '105dp',
		color:'#fff',
	
		
	});
	
	row.add(title);
	
	var textOfEvent=Ti.UI.createLabel({
		text: text,
		top:'30dp',
		left:'105dp',
		font:{fontSize:12},
		color:'#fff',
		
	});
	
	row.add(textOfEvent);
	

	dataArray[curr]=row;
	curr++;
};



getData = Ti.Network.createHTTPClient({});

getData.open("GET", "http://klintman.se/KlagshamnsskolanApp/kalender.json");


getData.onload = function() {

	var returnText = this.responseText;

	var returnData = eval(returnText);

	for ( i = 0; i < returnData.length; i++) {
		addToDataArray(returnData[i].Månad,returnData[i].Dag, returnData[i].rubrik, returnData[i].text);
	};

	calenderTableView.setData(dataArray);
};

getData.send();




























