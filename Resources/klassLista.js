var win = Ti.UI.currentWindow;


var b = Ti.UI.createLabel({
	text : 'Stäng',
	color : '#fff',
	top : '20dp',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function() {

	win.hide();
});

klasslista = [];
klasslista[0] = {
	title : 'Gröna Falken'
};
klasslista[1] = {
	title : 'Blåa Falken'
};
klasslista[2] = {
	title : 'Röda Falken'
};
klasslista[3] = {
	title : 'Gröna 1:or'
};
klasslista[4] = {
	title : 'Blåa 1:or'
};
klasslista[5] = {
	title : 'Röda 1:or'
};

var tableView = Ti.UI.createTableView({
	backgroundColor : 'transparent',
	top : '50dp'

});

win.add(tableView);

dataArray = [];

function addToData(className) {

	var row = Ti.UI.createTableViewRow({
		id:className,
		width : Ti.UI.FILL,
		height : "70",
		hasChild : true,
		selectedBackgroundColor: 'transparent',
		//selectedColor: 'black'

	});

	var klass = Ti.UI.createLabel({
		text : className,
		font : {
			fontSize : 34,
			fontWeight : 'bold',
			fontFamily:'Snell Roundhand'
		},
		color : '#fff',
	});

	row.add(klass);

	dataArray.push(row);
};

for ( i = 0; i < klasslista.length; i++) {

	addToData(klasslista[i].title);
};

tableView.data = dataArray;



tableView.addEventListener('click', function(e) {
	
	//alert(e.rowData.id);

	if (e.rowData.id == 'Gröna Falken') {

		var win = Ti.UI.createWindow({
			backgroundImage : 'images/LinenDark.png',
			url : 'klassListaSubwinGF.js'
		});
		win.open();
	};
	
	
	if (e.rowData.id == 'Blåa Falken') {

		var win = Ti.UI.createWindow({
			backgroundImage : 'images/LinenDark.png',
			url : 'klassListaSubwinBF.js'
		});
		win.open();
		
	};
	
	
	
	if (e.rowData.id== 'Röda Falken') {

		var win = Ti.UI.createWindow({
			backgroundImage : 'images/LinenDark.png',
			url : 'klassListaSubwinRF.js'
		});
		win.open();
	};
	
	
	if (e.rowData.id== 'Gröna 1:or') {

		var win = Ti.UI.createWindow({
			backgroundImage : 'images/LinenDark.png',
			url : 'klassListaSubwinG1.js'
		});
		win.open();
	
	};
	
	
	
	if (e.rowData.id== 'Blåa 1:or') {

		var win = Ti.UI.createWindow({
			backgroundImage : 'images/LinenDark.png',
			url : 'klassListaSubwinB1.js'
		});
		win.open();
		
	
	};
	
	
	
	if (e.rowData.id== 'Röda 1:or') {

		var win = Ti.UI.createWindow({
			backgroundImage : 'images/LinenDark.png',
			url : 'klassListaSubwinR1.js'
		});
		win.open();
		
	};


});


