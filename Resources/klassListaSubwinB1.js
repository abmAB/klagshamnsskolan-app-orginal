var win = Ti.UI.currentWindow;

var b = Ti.UI.createLabel({
	text : 'Tillbaka',
	color : '#fff',
	top : '20dp',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function() {

	win.close();
});

var dataArray = [];

var curr = 0;

function addToData(image, childName, momNumber, dadNumber) {

	var row = Ti.UI.createTableViewRow({

		name : childName,
		mom : momNumber,
		dad : dadNumber,
		height : '80dp',
		width : Ti.UI.FILL,
		//backgroundImage : 'images/tableViewRow2.png',
		selectedBackgroundColor:'transparent'
		

	});



	var image = Ti.UI.createImageView({
		image : image,
		//top : '5dp',
		left : '-100dp',
		height : '50dp',
		widht : '50dp',

	});
	
	row.add(image);
	
	var name = Ti.UI.createLabel({
		color:'#fff',
		text : childName,
		left : '55dp',
		font : {
			fontSize : 20,
			fontWeight : 'bold'
		},
		

	});

	row.add(name);

	var mom = Ti.UI.createLabel({
		color:'#fff',
		text : momNumber,
		width : 'auto',
		height : 'auto',
		top : '15',
		right : '10dp',
		font : {
			fontSize :16
		}

	});

	//row.add(mom);

	var dad = Ti.UI.createLabel({
		color:'#fff',
		text :dadNumber,
		width : 'auto',
		height : 'auto',
		bottom : '20dp',
		right : '10dp',
		font : {
			fontSize :16
		}

	});

	//row.add(dad);

	dataArray[curr] = row; 
	curr++;
};

// ************************************************************************
// ************************************************************************

var tableView = Ti.UI.createTableView({
	backgroundColor : 'transparent',
	top : '50dp',

});

win.add(tableView);

// ************************************************************************
// ************************************************************************

tableView.addEventListener('click', function(e) {

	var dialog = Ti.UI.createAlertDialog({
		buttonNames : ['Ring mamma', 'Ring pappa', 'Sms mamma', 'Sms pappa', 'Ångra'],
		title : e.row.name,
		mom : e.row.mom,
		dad : e.row.dad,

		//cancel : 3
	});

	// ************************************************************************
	// ************************************************************************

	var dialog2 = Ti.UI.createAlertDialog({
		buttonNames : ['Ring mamma', 'Sms mamma', 'Ångra'],
		title : e.row.name,
		message : e.row.mom,
		mom : e.row.mom,

	});
	// ************************************************************************
	// ************************************************************************

	var dialog3 = Ti.UI.createAlertDialog({
		buttonNames : ['Ring pappa', 'Sms pappa', 'Ångra'],
		title : e.row.name,
		message : e.row.dad,
		dad : e.row.dad,

	});
	// ************************************************************************
	// ************************************************************************

	if (e.row.mom !== '' && e.row.dad !== '') {

		dialog.addEventListener('click', function(e) {
			if (e.index === 0) {
				//alert(this.mom);
				Ti.Platform.openURL('tel:' + this.mom);
			}

			if (e.index === 1) {
			//	alert(this.dad);
				Ti.Platform.openURL('tel:' + this.dad);
			}

			if (e.index === 2) {
			//	alert(this.mom);
				Ti.Platform.openURL('sms:' + this.mom);
			}

			if (e.index === 3) {

				//alert(this.dad);
				Ti.Platform.openURL('sms:' + this.dad);
			}
		});

		dialog.show();

	};

	// ************************************************************************
	// ************************************************************************

	if (e.row.mom != '' && e.row.dad == '') {

		dialog2.addEventListener('click', function(e) {

			if (e.index === 0) {
				//alert(this.mom);
				Ti.Platform.openURL('tel:' + this.mom);
			}

			if (e.index === 1) {
				//alert(this.mom);
				Ti.Platform.openURL('sms:' + this.mom);
			}

		});

		dialog2.show();

	};

	// ************************************************************************
	// ************************************************************************

	if (e.row.mom == '' && e.row.dad != '') {

		dialog3.addEventListener('click', function(e) {

			if (e.index === 0) {
				//alert(this.dad);
				Ti.Platform.openURL('tel:' + this.dad);
			}

			if (e.index === 1) {
				//alert(this.dad);
				Ti.Platform.openURL('sms:' + this.dad);
			}

		});

		dialog3.show();

	};

});

// ************************************************************************
// ************************************************************************

var getData = Ti.Network.createHTTPClient({
	
	
	onload:function() {

	var returnData = JSON.parse(this.responseText);
	
	Ti.App.Properties.setString('offlineB1', this.responseText );

	
	
	for ( i = 0; i < returnData.length; i++) {
		addToData(returnData[i].bildurl, returnData[i].barnetsNamn, returnData[i].mammasNummer, returnData[i].pappasNummer);
	};
	
    tableView.data=dataArray;
	//tableView.setData(dataArray);
},
	
	
	
	onerror:function(e) {
		
if (Titanium.App.Properties.getString('offlineB1','')==''){
	
	alert('Första gången du startar appen måste du ha internet');
	
	return;
}


		 var returnData = JSON.parse(Titanium.App.Properties.getString('offlineB1',''));

	
	for ( i = 0; i < returnData.length; i++) {
		addToData(returnData[i].bildurl, returnData[i].barnetsNamn, returnData[i].mammasNummer, returnData[i].pappasNummer);

	};

	tableView.data=dataArray;
	
  },
});


	// function badinternet (e) {
// 	
	// var returnData = JSON.parse(Titanium.App.Properties.getString('offLine', ''));
// 	
// 	
// 	
	// for ( i = 0; i < returnData.length; i++) {
		// addToData(returnData[i].bildurl, returnData[i].barnetsNamn, returnData[i].mammasNummer, returnData[i].pappasNummer);
// 
	// };
// 
	// tableView.data=dataArray;
// 	
 // };
//  
// 
// badinternet();

getData.open("GET", "http://klintman.se/KlagshamnsskolanApp/klasslistaB1.json");


getData.send();

