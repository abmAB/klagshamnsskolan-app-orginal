var win = Ti.UI.currentWindow;

var b = Ti.UI.createLabel({
	text : 'Stäng',
	color : '#fff',
	top : '20dp',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function() {

	win.hide();
});



// ************************************************************************
// ************************************************************************




var dataArray = [];
var curr = 0;

function addToData(image, workTitle, name, cellphone, phone, email) {
	
	
// 	Skapar en rad med en presentationsbild, namn, nr och email. Raderna (row) får egenskaper som jag kan lyssna efter
//  när jag ska bestämma vilken AlertDialog som ska visas beroende om det finns mobil och fast nr eller det ena
// eller det andra.





	var row = Ti.UI.createTableViewRow({
		name : name,
		cellphone : cellphone,
		email : email,
		phone : phone,
		height : '90dp',
		width : '100%',
		backgroundColor:'transparent',
		selectedBackgroundColor:'transparent',
		//backgroundImage : 'images/tableViewRow2.png',

	});

	var img = Ti.UI.createImageView({
		image : image,
		top : '5dp',
		left : '-100dp',
		height : '55dp',
		widht : '55dp',

	});

	row.add(img);

	var title = Ti.UI.createLabel({
		color:'#fff',
		width : 'auto',
		height : 'auto',
		text : workTitle,
		top : '3dp',
		left : '65dp',
		font : {
			fontSize : 12
		}

	});

	row.add(title);

	var name = Ti.UI.createLabel({
		color:'#fff',
		width : 'auto',
		height : 'auto',
		text : name,
		top : '20dp',
		left : '65dp',
		font : {
			fontSize : 18,
			fontWeight : 'bold'
		}

	});

	row.add(name);

	var cellphone = Ti.UI.createLabel({
		color:'#fff',
		width : 'auto',
		height : 'auto',
		text : cellphone,
		top : '8dp',
		right : '10dp',
		font : {
			fontSize : 18
		}

	});

	row.add(cellphone);

	var phone = Ti.UI.createLabel({
		color:'#fff',
		width : 'auto',
		height : 'auto',
		text : phone,
		top : '28dp',
		right : '10dp',
		font : {
			fontSize : 18
		}

	});

	row.add(phone);

	var email = Ti.UI.createLabel({
		color:'#fff',
		width : 'auto',
		height : 'auto',
		text : email,
		left : '65dp',
		bottom : '25dp',
		font : {
			fontSize : 14
		}

	});

	row.add(email);

	dataArray[curr] = row;
	curr++;
};



// ******************************************************************************
// ******************************************************************************



var tableView = Ti.UI.createTableView({
	backgroundColor : 'transparent',
	separatorStyle : Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
	top : '50dp',

});

win.add(tableView);



tableView.addEventListener('click', function(e) {

	var dialog = Ti.UI.createAlertDialog({
		buttonNames : ['Ring mobil', 'Ring fast nr', 'Sms', 'E-mail', 'Ångra'],
		title : e.row.name,
		cellphone : e.row.cellphone,
		phone : e.row.phone,
		email : e.row.email,
		//cancel : 3
	});

	var dialog2 = Ti.UI.createAlertDialog({
		buttonNames : ['Ring mobil', 'Sms', 'E-mail', 'Ångra'],
		title : e.row.name,
		message : e.row.cellphone,
		cellphone : e.row.cellphone,
		email : e.row.email,

	});

	var dialog3 = Ti.UI.createAlertDialog({
		buttonNames : ['Ring', 'E-mail', 'Ångra'],
		title : e.row.name,
		message : e.row.phone,
		phone : e.row.phone,
		email : e.row.email,
	});




// ************************************************************************
// ************************************************************************



	if (e.row.cellphone != '' && e.row.phone != '') {

		dialog.addEventListener('click', function(e) {

			if (e.index === 0) {
				//alert(this.cellphone);
				Ti.Platform.openURL('tel:' + this.cellphone);
			}

			if (e.index === 1) {
				//alert(this.phone);
				Ti.Platform.openURL('tel:' + this.phone);
			}

			if (e.index === 2) {
				//alert(this.cellphone);
				Ti.Platform.openURL('sms:' + this.cellphone);
			}

			if (e.index === 3) {

				var emailDialog = Ti.UI.createEmailDialog();
				emailDialog.subject = '';
				emailDialog.toRecipients = [this.email];
				emailDialog.open();
			}
		});

		dialog.show();

	};


// ************************************************************************
// ************************************************************************




	if (e.row.cellphone != '' && e.row.phone == '') {

		dialog2.addEventListener('click', function(e) {

			if (e.index === 0) {
				//alert(this.cellphone);
				Ti.Platform.openURL('tel:' + this.cellphone);
			}

			

			if (e.index === 1) {
				//alert(this.cellphone);
				Ti.Platform.openURL('sms:' + this.cellphone);
			}

			if (e.index === 2) {

				var emailDialog = Ti.UI.createEmailDialog();
				emailDialog.subject = '';
				emailDialog.toRecipients = [this.email];
				emailDialog.open();
			}
		});

		dialog2.show();

	};



// ************************************************************************
// ************************************************************************




	if (e.row.cellphone == '' && e.row.phone != '') {

		dialog3.addEventListener('click', function(e) {

			if (e.index === 0) {
				//alert(this.phone);
				Ti.Platform.openURL('tel:' + this.phone);
			}


			if (e.index === 1) {

				var emailDialog = Ti.UI.createEmailDialog();
				emailDialog.subject = '';
				emailDialog.toRecipients = [this.email];
				emailDialog.open();
			}
		});

		dialog3.show();

	};

});


// ************************************************************************
// ************************************************************************




var getData = Ti.Network.createHTTPClient({
	
	onload: function() {

	var returnData = JSON.parse(getData.responseText);
	
	Ti.App.Properties.setString('kontaktOnError', getData.responseText );

	for ( i = 0; i < returnData.length; i++) {
		addToData(returnData[i].bildurl, returnData[i].workTitle, returnData[i].name, returnData[i].cellphone, returnData[i].phone, returnData[i].email);
	}

	tableView.data=dataArray;
	//
	

},

onerror:function(e) {
	
	var returnData = JSON.parse(Titanium.App.Properties.getString('kontaktOnError','No internet, try again'));
	
	
	for ( i = 0; i < returnData.length; i++) {
		addToData(returnData[i].bildurl, returnData[i].workTitle, returnData[i].name, returnData[i].cellphone, returnData[i].phone, returnData[i].email);
	}

	tableView.data=dataArray;
	
  },
	
	
	
});

getData.open("GET", "http://klintman.se/KlagshamnsskolanApp/kontakterFinal.json");

getData.send();

