var win = Ti.UI.currentWindow;

// var webView = Ti.UI.createWebView({
// width : '475dp',
// height : '700dp',
// top : '0dp',
// url : "http://meny.dinskolmat.se/klagshamnsskolan/"
//
// });
//
// win.add(webView);
var date;
var days;

function createDate() {

	var d = new Date();
	
	//Ti.API.info(String.formatDate(d, 'short'));
	
	currDay = d.getDate();
	//15   //131015
	currMonth = d.getMonth() + 1;
	//10
	currYear = d.getFullYear();
	//13
	currH = d.getHours();
	currM = d.getMinutes();
	currS = d.getSeconds();

	if (currDay < 10 ){
		currDay = '0' + currDay;
	}
	if (currMonth < 10 ){
		currMonth = '0' + currMonth;
	}

	date = currYear + '-' + currMonth + '-' + currDay;
	//2013-10-15

}



createDate();
Ti.API.info(date);

var b = Ti.UI.createLabel({
	text : 'Stäng',
	color : '#fff',
	top : '20p',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function(e) {

	win.hide();
});

//************************************************
// *************************************************

var myData = [];
var curr = 0;

var tableView = Ti.UI.createTableView({
	backgroundColor : 'transparent',
	separatorColor : '#fff',
	top : '50dp',

});

win.add(tableView);

//  *************************************************************
// **************************************************************

function addToData(days, date, content, color) {

	var row = Ti.UI.createTableViewRow({

		height : 'auto',
		width : '100%',
		selectedBackgroundColor : 'transparent',

	});

	days = Ti.UI.createLabel({
		text : days,
		color : color,
		left : '5dp',
		//bottom : '5dp',
		font : {
			fontSize : 30,
			fontFamily : 'Snell roundhand',
			fontWeight : 'bold'
		},

	});

	row.add(days);

	var date = Ti.UI.createLabel({
		color : '#fff',
		text : date,
		left : '5dp',
		top : '5dp',
		font : {
			fontSize : 12
		},

	});

	row.add(date);

	var text = Ti.UI.createLabel({
		color : '#fff',
		width : 'auto',
		height : 'auto',
		text : content,
		top : '10dp',
		bottom : '15dp',
		left : '85dp',
		right : '10dp',
		font : {
			fontSize : 18,
			fontWeight : 'bold',
			fontFamily : 'Snell Roundhand'
		}

	});

	row.add(text);

	myData[curr] = row;
	curr++;
};

// ******************************************************************************
// ********************************************************************



var getData = Ti.Network.createHTTPClient({

});
getData.open("GET", "http://meny.dinskolmat.se/klagshamnsskolan/");

getData.setRequestHeader("Accept", "application/json");
getData.setRequestHeader("Accept-Encoding", "gzip");

getData.onload = function() {

	json = JSON.parse(this.responseText);

	var day = '';
	var color = '';

	for ( i = 0; i < json.weeks[0].days.length; i++) {

		if (i == 0) {

			day = 'Mån';
		} else if (i == 1) {
			day = 'Tis';
		} else if (i == 2) {

			day = 'Ons';
		} else if (i == 3) {

			day = 'Tors';
		} else if (i == 4) {

			day = 'Fre';
		}

		// Ti.API.info(json.weeks[0].days[i].date);
		// Ti.API.info(date);

		if (json.weeks[0].days[i].date == date) {

			day = 'Idag';
			color = 'orange';

		} else
			( color = '#fff');

		//om dagen[i] != null
		if (json.weeks[0].days[i].items != null) {

			if (json.weeks[0].days[i].reason != null) {

				addToData(day, json.weeks[0].days[i].date, json.weeks[0].days[i].reason, color);
			}

			//Ti.API.info('items' + json.weeks[0].days[i].items.length);


			foodText = "";
			for(j = 0;j < json.weeks[0].days[i].items.length;j++)
			{
				foodText = foodText + json.weeks[0].days[i].items[j] + " \n\n";
			}
          

           // addToData(day, '', '', color);
			addToData(day, json.weeks[0].days[i].date, foodText, color);
/*
			if (json.weeks[0].days[i].items.length == 2) {
				addToData(day, json.weeks[0].days[i].date, foodText, color);
			}

			// om det är fler än 2 maträtter
			else {
				addToData(day, json.weeks[0].days[i].date, json.weeks[0].days[i].items[0] + '\n\n ' + json.weeks[0].days[i].items[1] + ' \n\n' + json.weeks[0].days[i].items[2], color);

			}
			*/
		}

	}

	tableView.setData(myData);
};

getData.send();

