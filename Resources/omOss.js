var win = Ti.UI.currentWindow;

var b = Ti.UI.createLabel({
	text : 'Stäng',
	color : '#fff',
	top : '20dp',
	right : '10dp',
	width : 'auto',
	height : 'auto',
});

win.add(b);

b.addEventListener('click', function() {

	win.hide();
});

var scrollView = Ti.UI.createScrollView({
	contentWidth : '100%',
	// contentHeight: 'auto',
	showVerticalScrollIndicator : true,
	backgroundColor : '#fff',
	height : 'auto',
	width : '320dp',
	layout : 'vertical',
	top : '50dp'
});
// var view = Ti.UI.createView({
// layout:'vertical',
// backgroundColor:'#fff',
// borderRadius: 10,
// top: 10,
// height:Titanium.UI.SIZE,
// width:'300dp'
// });
// scrollView.add(view);
win.add(scrollView);

var title = Ti.UI.createLabel({
	top : '20dp',
	text : 'Om skolan',
	font : {
		fontSize : 22,
		fontWeight : 'bold'
	},
});

scrollView.add(title);

var view1 = Ti.UI.createImageView({
	image : 'images/klagshamnsskolan2.png',
	width : Titanium.UI.FILL,
	height : Titanium.UI.FILL,
});
var view2 = Ti.UI.createImageView({
	image : 'images/gronFlagg.png',
	width : Titanium.UI.FILL,
	height : Titanium.UI.FILL,
});
var view3 = Ti.UI.createImageView({
	image : 'images/karta.png',
	width : Titanium.UI.FILL,
	height : Titanium.UI.FILL,
});

var scrollableView = Ti.UI.createScrollableView({
	top : '10dp',
	width : '280dp',
	height : '160dp',
	pagingControlHeight : '10dp',

	//pagingControlColor: '#d8d8d8',
	views : [view1, view2, view3],
	showPagingControl : true,

});

scrollView.add(scrollableView);

var text = Ti.UI.createLabel({
	left : '10dp',
	right : '10dp',
	top : '20dp',
	width : 'auto',
	font : {
		fontSize : 20,
		fontWeight : 'bold'
	},
	text : 'Klagshamnsskolans ledord i arbetet med barn och unga är kunskap-glädje-trygghet-självständigt tänkande.'

});

scrollView.add(text);

var text2 = Ti.UI.createLabel({
	left : '10dp',
	right : '10dp',
	top : '20dp',
	width : 'auto',
	font : {
		fontSize : 16
	},
	text : 'Klagshamnsskolan är en liten skola, där alla känner alla. Gamla skolan är över 100 år, men den sprudlar av liv och aktiva elever.För att öka läsintresset lägger vi ut 30 minuter extra läsning varje vecka. Klagshamnsskolan är en skola med många traditioner och gemensamma temadagar, då vi har blandade åldersgrupper. Vartannat år bjuds föräldrarna in till Öppet hus, då eleverna visar upp sin skola och berättar om sitt skolarbete.'
});

scrollView.add(text2);
//
// var textArea= Ti.UI.createTextArea({
// top:'40dp',
// width:'300dp',
// height:'500dp',
// backgroundColor: '#fff',
// borderRadius:10,
// opacity:0.8,
// value:
//
// });
//
// win.add(textArea);
// var view1 = Ti.UI.createView({ backgroundColor:'#123' });
// var view2 = Ti.UI.createView({ backgroundColor:'#246' });
// var view3 = Ti.UI.createView({ backgroundColor:'#48b' });
//
// var scrollableView = Ti.UI.createScrollableView({
// views:[view1,view2,view3],
// showPagingControl:true
// });
//
// win.add(scrollableView);