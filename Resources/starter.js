Ti.include('iconSettings.js');

var win = Ti.UI.currentWindow;

var win1 = Ti.UI.createWindow({
	backgroundImage : 'images/LinenDark.png',
	url : 'kontakter.js'
});
win1.open();
win1.hide();

var win2 = Ti.UI.createWindow({
	backgroundImage : 'images/LinenDark.png',
	url : 'klassLista.js',
});
win2.open();
win2.hide();

var win3 = Ti.UI.createWindow({
backgroundImage:'images/LinenDark.png',
url : 'matsedel.js',
});
win3.open();
win3.hide();

var win4 = Ti.UI.createWindow({
	backgroundImage : 'images/LinenDark.png',
	url : 'kalender.js',
});
win4.open();
win4.hide();

var win5 = Ti.UI.createWindow({
	backgroundImage : 'images/LinenDark.png',
	url : 'fb.js',
});
win5.open();
win5.hide();

var win6 = Ti.UI.createWindow({
	backgroundImage : 'images/LinenDark.png',
	url : 'omOss.js',
});
win6.open();
win6.hide();

var win7 = Ti.UI.createWindow({
	backgroundImage : 'images/LinenDark.png',
	url : 'feedBack.js',
});
win7.open();
win7.hide();

var title = Titanium.UI.createLabel({
	color : '#DAA520',
	text : 'Klagshamns Skolan',
	top : '18dp',
	font : {
		fontSize : 37,
		fontFamily : 'Snell Roundhand',
		fontWeight : 'bold',
	},
	textAlign : 'center',

});


var clearButton = Ti.UI.createView({
	bubbleParent : false,
	top : '132dp',
	right : '25dp',
	height : '25dp',
	width : '50',
	borderRadius : 5,
	backgroundColor : '#D8D8D8',

});

var clearLabel = Ti.UI.createLabel({
	text : 'Rensa',
	color : '#474747',
	font : {
		fontSize : 12,
		fontWeight : 'bold',
	}
});

clearButton.add(clearLabel);
clearButton.hide();

var hint = Ti.UI.createLabel({
	text : 'Skriv något att komma ihåg...',
	font : {
		fontFamily : 'Snell Roundhand',
		fontSize : 20,
		fontWeight : 'bold'
	},
	color : 'gray',
	height : 'auto',
	width : 'auto',
	backgroundColor : 'transparent',
	touchEnabled : true
});

clearButton.addEventListener('click', function() {
	textArea.value = '';

	Titanium.App.Properties.setString('txtfld', "");
	hint.show();
	clearButton.hide();

});

var textArea = Ti.UI.createTextArea({
	backgroundColor : '#fff',
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	color : '#474747',
	top : '65dp',

	width : '300dp',
	height : '65dp',
	bubbleParent : false,
	borderWidth : '3dp',
	borderColor : '#474747',
	borderRadius : 10,
	font : {
		fontSize : 16,
		fontWeight : 'bold'
	},
	keyboardType : Ti.UI.KEYBOARD_DEFAULT,
	autocapitalization : Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES,
	textAlign : 'left',
	value : Ti.App.Properties.getString('txtfld', ''),
	suppressReturn : false,

});

textArea.add(hint);

if (textArea.value.length > 0) {
	hint.hide();
	
}

//*** Hint text appears when TextArea is blank, disappears when text is entered
textArea.addEventListener('change', function(e) {
	if (e.source.value.length > 0) {
		hint.hide();
		clearButton.show();
	} else {
		hint.show();
	}
});

textArea.addEventListener('focus', function(e) {
	if (e.source.value.length > 0) {

		clearButton.show();
	}
});

textArea.addEventListener('blur', function(e) {
	if (e.source.value.length == 0) {
		hint.show();
	}

	clearButton.hide();

});
//*** Make sure that TextArea gets focus if they tap the hint label
hint.addEventListener('click', function(e) {
	textArea.focus();
});

win.addEventListener('click', function(e) {
	clearButton.hide();
	textArea.blur();
});


textArea.addEventListener('change', function(e) {

	//clearButton.show();
	Titanium.App.Properties.setString('txtfld', e.value);
});

win.add(textArea);

win.add(clearButton);

kontakt.add(iconImage);
kontakt.add(label);
win.add(kontakt);

kontakt.addEventListener('click', function() {

	win1.show();
	// var win = Ti.UI.createWindow({
	// backgroundImage: 'images/LinenDark.png',
	// url : 'kontakter.js'
	// });
	// win.open();
});

klasslista.add(iconImage2);
klasslista.add(label2);
win.add(klasslista);

klasslista.addEventListener('click', function() {

	function logIn() {
		
		
		var txtfield = Ti.UI.createTextField({

				value : Ti.App.Properties.getString('txtfield', ''),
				backgroundColor : '#E7A959',
				width : '250dp',
				height : '70dp',
				top : '150dp',
				//borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
				borderRadius : 10,
				clearButtonMode : Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
				rightButtonMode : Ti.UI.NPUT_BUTTONMODE_ONFOCUS,
				bubbleParent : false,
				textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,

			});

		if (txtfield.value == 'Klagshamn rocks') {

			win2.show();
			
		} else {

			var logInWin = Ti.UI.createWindow({

				backgroundImage : 'images/LinenDark.png'

			});

			logInWin.open();

			var closeButton = Ti.UI.createLabel({
				text : 'Stäng',
				color : '#fff',
				right : '15dp',
				width : 'auto',
				height : 'auto',
				top : '15dp'
			});

			closeButton.addEventListener('click', function() {

				logInWin.close();
			});

			logInWin.add(closeButton);

			

			logInWin.add(txtfield);

			var hintPassWord = Ti.UI.createLabel({

				text : 'Lösenord...',
				font : {
					fontFamily : 'Snell Roundhand',
					fontSize : 24,
					fontWeight : 'bold'
				},
				color : 'black',
				height : 'auto',
				width : 'auto',
				backgroundColor : 'transparent',
				touchEnabled : true,

			});

			txtfield.add(hintPassWord);

			txtfield.addEventListener('focus', function() {

				hintPassWord.hide();

			});

			txtfield.addEventListener('blur', function(e) {
				if (e.source.value.length == 0) {
					hintPassWord.show();
				} else {
					hintPassWord.hide();
				}
			});
			//*** Make sure that TextArea gets focus if they tap the hint label
			hintPassWord.addEventListener('click', function(e) {
				txtfield.focus();
			});

			logInWin.addEventListener('click', function(e) {
				txtfield.blur();
			});

			txtfield.addEventListener('return', function(e) {

				if (e.value == 'Klagshamn rocks') {

					logInWin.close();
					Titanium.App.Properties.setString('txtfield', e.value);
					win2.show();

				} else {
					alert('Oops, fel lösenord');

				}

			});

		}
	}

	
	
	logIn();
});



// var win = Ti.UI.createWindow({
// backgroundImage : 'images/LinenDark.png',
// url : 'klassLista.js',
// });
// win.open();
// });

matSedel.add(iconImage3);
matSedel.add(label3);
win.add(matSedel);

matSedel.addEventListener('click', function() {
	
win3.show();
	
});





// var win = Ti.UI.createWindow({
// backgroundColor : '#fff',
// url : 'matsedel.js',
// });
// win.open();
// });

calender.add(iconImage4);
calender.add(label4);
win.add(calender);

calender.addEventListener('click', function() {

	win4.show();

	// var win = Ti.UI.createWindow({
	// backgroundImage: 'images/LinenDark.png',
	// url : 'kalender.js',
	// });
	// win.open();
});

// fb.add(iconImage5);
// fb.add(label5);
// win.add(fb);
// 
// fb.addEventListener('click', function() {
// 
	// win5.show();

	// var win = Ti.UI.createWindow({
	// backgroundImage:'images/LinenDark.png',
	// url : 'fb.js',
	// });
	// win.open();
//});

omOss.add(iconImage6);
omOss.add(label6);
win.add(omOss);

omOss.addEventListener('click', function() {

	win6.show();

	// var win = Ti.UI.createWindow({
	// backgroundImage:'images/LinenDark.png',
	// top:0,
	// url : 'omOss.js',
	// });
	// win.open();
});

feedBack.add(iconImage7);
feedBack.add(label7);
win.add(feedBack);

feedBack.addEventListener('click', function() {

	win7.show();

	// var win = Ti.UI.createWindow({
	// backgroundImage:'images/LinenDark.png',
	// url : 'feedBack.js',
	// });
	// win.open();
});


var infoArray=[];
var curr=0;



function infoText(infoText){
	
	var row=Ti.UI.createTableViewRow({
		height: '70dp',
		selectedBackgroundColor: 'transparent',
		
	});
	
	
	var label = Ti.UI.createLabel({
		left:'10dp',
		right:'10dp',
		

	text : infoText,
	font : {
		fontFamily : 'Snell Roundhand',
		fontSize : 20,
		fontWeight : 'bold'
	},
	color : '#fff'

});

row.add(label);


	infoArray[curr] = row;
	curr++;
	
};
var infoTableView=Ti.UI.createTableView({
	
	backgroundColor:'transparent',
	top:'355dp',
	height:'120dp',
	separatorStyle : Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
	
});


win.add(infoTableView);


var getData = Ti.Network.createHTTPClient({});

getData.open("GET", "http://klintman.se/KlagshamnsskolanApp/infoText.json");

getData.onload = function() {


	var returnText = this.responseText;

	var returnData = eval(returnText);
	
	

	for ( i = 0; i < returnData.length; i++) {
		infoText(returnData[i].infoText);
	};

	infoTableView.setData(infoArray);
};

getData.send();

win.add(title);



